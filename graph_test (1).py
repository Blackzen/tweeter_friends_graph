import matplotlib.pyplot as plt
import networkx as nx
import pandas as pd
import ast
import random 
import numpy as np


G=nx.Graph()

#df = pd.read_csv('friends.csv',names= ['id','name','followers_count','friends'])
#df2 = pd.read_csv('all_friends_ids.csv',names = ['id','friends'])


#print( df2.head())

#test=(df[['id','friends']]).append(df2[['id','friends']].iloc[1:])

#test.to_csv('last_list.csv')

#G.add_nodes_from(df.id)

test = pd.read_csv('last_friends.csv',names= ['id','name','followers_count','friends'])
amigos=test['id']
amigos_test=test['id'].tolist()

#print ( type(amigos_test))
#print amigos

G.add_nodes_from(amigos)
edge_list = [] 
ids_list = [] 
counter = 0 

#print(test.head())
#####tiene problemas si se cuenta hasta el ultimo elemento de la lista por eso el -1
for i in range(1586):
	#print(test['friends'][i])
	temp=ast.literal_eval(test['friends'][i])
	
	# B=set(temp)
	# A=set(amigos)
	interseccion=np.intersect1d(amigos_test, temp)
	

	for k in interseccion: 		
		edge_list.append((test.id[i],k)) 

print(edge_list)

####aca me doy cuenta que si un nodo no existe lo crea dependiendo de si la arista si exista 
G.add_edges_from(edge_list)

#remove = [node for node,degree in G.degree(G.nodes()) if degree < 2]

remove = []
for node in G.nodes():
    if G.degree(node) <=2:
        remove.append(node)

G.remove_nodes_from(remove)

#print G.nodes()

###aqui ya tengo la lista de todos los usuarios con sus respectivos grados dependiendo del grafo generado
lista_inlfuyentes=list(G.degree(G.nodes()))

###el mas inlfluyente 
inlfuyente =max(lista_inlfuyentes,key=lambda item:item[1])

print ("identificador mas influyente "+ str(inlfuyente[0]) + " seguidores "+str(inlfuyente[1]))

lista_inlfuyentes=sorted(lista_inlfuyentes, key=lambda element: (element[1]))


###mas inlufyebtes ordenados por lista
for ids in lista_inlfuyentes:
	
	print ("identificador "+ str(ids[0]) + " seguidores "+str(ids[1]))

color_map = []
for node in G.nodes():
    if G.degree(node) >300:
        color_map.append('red')
    else: color_map.append('gray')   

nx.draw(G,pos=nx.spring_layout(G),node_color=color_map,node_size=10,edge_color='#A0CBE2') 

plt.show()